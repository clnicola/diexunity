﻿using UnityEngine;
using System.Collections;

public static class Manager : object {
	
	public static GameObject map;
	public static GameObject bp;
	public static GameObject gp;

	/// <summary>
	/// Creates the random vector3 that is used in other classes.
	/// The Vector3 is contained in the map.
	/// </summary>
	/// <returns>The random vector3.</returns>
	public static Vector3 createRandomVector3(){
		float mapX, mapZ, mapY;
		mapX = Random.Range(map.transform.localScale.x * -5,map.transform.localScale.x * 5);
		mapY = (float)1;
		mapZ = Random.Range(map.transform.localScale.z * -5,map.transform.localScale.z * 5);
		Vector3 random = new Vector3 (mapX, mapY, mapZ);
		return random;
	}
	
}
