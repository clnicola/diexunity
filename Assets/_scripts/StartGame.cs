﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartGame : MonoBehaviour {

	public GameObject rules;
	public GameObject highScores;
	public GameObject credits;
	public InputField playerNameField;
	public Text t;
	public Toggle leapToggle;
	

	public void Start(){
		leapEnable ();
		collapseAll ();
		viewHighScores ();
	}

	public void startGame(string sceneName){
		Application.LoadLevel (sceneName);
	}

	public void exitGame(){
		Application.Quit ();

	}

	public void viewHighScores(){
		collapseAll ();
		highScores.SetActive (true);
		showHighScore ();

	}

	public void viewRules(){
		collapseAll ();
		rules.SetActive (true);
		
	}

	public void viewCredits(){
		collapseAll ();
		credits.SetActive (true);
		
	}
	/// <summary>
	/// Enables the Leap Motion sensor according to the checkbox on the main menu.
	/// </summary>
	public void leapEnable(){
		Debug.LogWarning("enter leapenable func");
		bool enabled = leapToggle.isOn;
		if(enabled)
			PlayerPrefs.SetInt ("LeapToggle", 1);
		else
			PlayerPrefs.SetInt ("LeapToggle", 0);
	}
	public void collapseAll(){
		highScores.SetActive (false);
		rules.SetActive (false);
		credits.SetActive (false);
	}
	/// <summary>
	/// Sets the name of the player.
	/// </summary>
	public void setPlayerName(){
		PlayerPrefs.SetString ("playerName", playerNameField.text);
	//	Debug.LogWarning ("playerName key set with " + playerNameField.text);
	}

	void showHighScore(){
		for (int i = 1; i<=5; i++) {
			//Debug.LogWarning("player " + i + " Has score " +PlayerPrefs.GetInt (i+"SurvivalScore"));
			if (PlayerPrefs.HasKey(i+"SurvivalScore")){
				t = GameObject.Find(i+"PlayerTxt").GetComponent<Text>();
				t.text = PlayerPrefs.GetString (i+"SurvivalName")
					+ "  " 
					+ PlayerPrefs.GetInt (i+"SurvivalScore");
			}
		}
	}

}
