/******************************************************************************\
* This is a Singleton class that emulates the axis input that most games have *
* Version 0.2
* Made it work with BootCamp Tutorial
* introduced a GetHandAxisStep for use in navigation (where normally the keybaord is used)
*
* Version 0.1
* initially published as part of LeanEnabledCarTutorial
* http://pierresemaan.com/leap-enabling-the-unity3d-car-tutorial/
\******************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

/// <summary>

/// </summary>
public static class LeapInput 
{	
	/*
	private enum HandID : int
	{
		Primary		= 0,
		Secondary	= 1
	};*/
	
	//Create a new leap controller object when you create this static class 
	static Leap.Controller 		controller	= null;
	static Leap.Frame			frame			= null;
	static Leap.Hand			hand			= null;
	static int					numFingers			= 0;
	static string m_Errors 			= "";

	// constructor called when the first class or member is referenced.
	static LeapInput()
	{
		try
		{
			//Create a new leap controller object when you create this static class 
			controller	= new Leap.Controller();
		}
		catch 
		{
			m_Errors = m_Errors + '\r' + '\n'  + "Controller could not be created";
		}
	}
	
	public static Leap.Frame Frame
	{
		get 		
		{ 
			Update(); return frame; 
		}
	}
	public static int FingersCount
	{
		get 		
		{ 
			Update();
			return numFingers; 
		}
	}
	
	public static Leap.Hand Hand
	{
		
		get 
		{ 
			Update();
			return hand; 
		}
	}
	
	public static string Errors
	{
		get { return m_Errors; }
	}
	
	public static void Update() 
	{	
		hand = null;
		frame = null;
		numFingers = 0;
		if( controller != null )
		{
			frame	= controller.Frame();
			if (frame != null)
			{
				if (frame.Hands.Count > 0) hand = frame.Hands[0];	
				//if (frame.Fingers.Count > 0) numFingers = frame.Fingers.Count;
				updateExtendedFingers();
			}
		}
	}

	public static void updateExtendedFingers(){
		int result = 0;
		if (hand != null) {
			for (int i=0; i<hand.Fingers.Count; i++) {
				Finger f = hand.Fingers [i];
				if (f.IsExtended) {
					result++;
				}
			}
		}
		Debug.LogWarning (result);
		numFingers = result;

	}
	// returns the hand axis scaled from -1 to +1
	public static float GetHandAxis(string axisName)
	{
		float ret = GetHandAxisPrivate(axisName);
		return ret;
	}
	
	public static bool GetHandGesture(string gestureName)
	{
		// Call Update so you can get the latest frame and hand
		Update();
		bool ret = false;
		if (hand != null)
		{
			switch (gestureName)
			{
			case "ZeroFingers":
				ret = numFingers == 0; 
				break;
			case "TwoFingers":
				ret =  numFingers == 2; 
				break;
			case "FiveFingers":
				ret =  numFingers == 5; 
				break;
			default:
				break;
			}
		}
		return ret;
	}
	
	private static float GetHandAxisPrivate(string axisName)
	{
		// Call Update so you can get the latest frame and hand
		Update();
		float ret = 0.0F;
		if (hand != null)
		{
			Vector3 PalmPosition = new Vector3(0,0,0);
			Vector3 PalmNormal = new Vector3(0,0,0);
			Vector3 PalmDirection = new Vector3(0,0,0);

			PalmPosition = hand.PalmPosition.ToUnityTranslated();
			PalmNormal = hand.PalmNormal.ToUnity();				
			PalmDirection = hand.Direction.ToUnity();

			
			switch (axisName)
			{
			case "Horizontal":
				ret =  PalmPosition.x ; 
				break;
			case "Vertical":
				ret =  PalmPosition.y ; 
				break;
			case "Depth":
				ret =  PalmPosition.z;
				break;
			case "Rotation":
				ret = -2 * PalmNormal.x ;
				break;
			case "Tilt":
				ret = PalmNormal.z ;
				break;
			case "HorizontalDirection":
				ret = PalmDirection.x ;
				break;
			case "VericalDirection":
				ret = PalmDirection.y ;
				break;
			default:
				break;
			}

		}
		return ret;
	}
	
}