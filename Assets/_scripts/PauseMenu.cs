﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	private bool gamePause = false;

	/// <summary>
	/// Resumes the game.
	/// </summary>
	public void resumeGame(){
		Time.timeScale = 1;
		gameObject.SetActive (false);
	}
	/// <summary>
	/// Gos to main menu.
	/// </summary>
	public void goToMainMenu(){
		Time.timeScale = 1;
		Application.LoadLevel ("mainMenu");	
	}


}
