﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreditScroll : MonoBehaviour {
	
	public GameObject title, subTitle;
	public GameObject name1, references;
	public GameObject text;
	public int speed = 1;
	
	/// <summary>
	/// Start this instance. Sets position of all texts lower at each loop.
	/// </summary>
	void Start(){

		int step = 90;
		for(int i=1; i<=transform.childCount; i++){
			text = GameObject.Find (i + "Credit");
			float f = -step * i;
			text.transform.localPosition = new Vector3(0, f , 0.0F );
		}
		
	}
	/// <summary>
	/// Updates position of text, making the texts scrool upwards.
	/// </summary>
	void Update () {
		for(int i=1; i<=transform.childCount; i++){
			text = GameObject.Find (i + "Credit");
			text.transform.Translate(Vector3.up * Time.deltaTime * speed);
		}
		
		
	}
	
	IEnumerator wait(){
		yield return new WaitForSeconds (20);
		
	}
}
