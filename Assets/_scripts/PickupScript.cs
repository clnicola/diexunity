﻿using UnityEngine;
using System.Collections;

public class PickupScript : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
		// Makes all pickups start in a Random position
		transform.position = Manager.createRandomVector3 ();
	}

	/// <summary>
	/// Updates Pickups and BadPickups, rotating them a certain degree. 
	/// Also moves BadPickup object around the map.
	/// </summary>
	void Update () {
		if(tag == "Pickup") 
			transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime); 

		else if (tag == "BadPickup") {
			transform.Rotate (new Vector3 (0, 10, 0) * Time.deltaTime);
			transform.Translate(Vector3.forward * Time.deltaTime*10);		
		}

	}


}
