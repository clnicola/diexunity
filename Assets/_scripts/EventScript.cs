﻿using UnityEngine;
using System.Collections;

public class EventScript : MonoBehaviour {

	GameObject panel;
	// Use this for initialization
	void Start () {
		panel = GameObject.Find("PausePanel");
		panel.SetActive (false);
	}
	
	/// <summary>
	/// Checks if the player has paused the game via ESC key or LeapMotion.
	/// Activates/deactivates PausePanel of the scene accordingly.
	/// </summary>
	void Update () {
		if(LeapInput.GetHandGesture("ZeroFingers")){
			panel.SetActive(true);
			Time.timeScale = 0;
		}
		else if(LeapInput.GetHandGesture("FiveFingers")){
			panel.SetActive(false);
			Time.timeScale = 1;
		}
		else if (Input.GetKeyDown (KeyCode.Escape)) {
			if(panel.gameObject.activeSelf)
			{
				panel.SetActive(false);
				Time.timeScale = 1;
			}
			else
			{
				panel.SetActive(true);
				Time.timeScale = 0;
			}
		}

	}
}
