﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	

	public float speed;
	public GUIText countText, winText;
	private int score;
	private const double minSize = 0.7;
	public int LeapEnabled;
	public int enemyCount;
	public int maxEnemyCount = 30;
	public int maxGoodPickup = 10;
	public string playerName; 

	public AudioSource pickupSound, badpickupSound, bounceSound; 
	// ANDROID ONLY
	Vector3 zeroAcceleration;
	Vector3 currentAcceleration;

	/// <summary>
	/// Resets the axes.
	/// </summary>
	void ResetAxes(){
		zeroAcceleration = Input.acceleration;
		currentAcceleration = Vector3.zero;
	}
	// 
	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake(){
		Manager.map = GameObject.Find("Ground");
		Manager.bp = GameObject.Find("BadPickup");
		Manager.gp = GameObject.Find("Pickup");
	}
	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start(){
		ResetAxes (); // ANDROID ONLY
		playerName = PlayerPrefs.GetString ("playerName", "JohnSmith");
		LeapEnabled = PlayerPrefs.GetInt ("LeapToggle");
		Debug.LogWarning ("Player name : " + playerName);
		//Init game variables
		enemyCount = 0;
		score = 0;
		//Init manager variables

		// Instantiate Pickups
		InstantiateGoodPickup ();
		InvokeRepeating("InstantiateBadPickup", 5, 5);

		//Update Score & UI Text

		SetScoreText();
		winText.text = "";
		ScoreUpdate();
	}
	/// <summary>
	/// Updates every second, and controls the movement
	/// </summary>
	 void FixedUpdate(){
		#if UNITY_ANDROID
		moveAndroid ();
		#elif UNITY_STANDALONE
		if(LeapEnabled == 0) moveKeyboard();
		if(LeapEnabled == 1) moveLeap ();
		#endif

	}

	/// <summary>
	/// Updates the score
	/// </summary>
	void ScoreUpdate(){
		InvokeRepeating ("IncreaseScore", 1, 1);
	}

	/// <summary>
	/// Increases the score.
	/// </summary>
	void IncreaseScore(){
		score++;
		SetScoreText ();
	}
	/// <summary>
	/// Moves the player for android.
	/// </summary>
	void moveAndroid(){
		float sensH = 10;
		float sensV = 10;
		float smooth = (float)0.5;

		currentAcceleration = Vector3.Lerp(currentAcceleration, Input.acceleration-zeroAcceleration, Time.deltaTime/smooth);
		float androidMoveVertical = Mathf.Clamp(currentAcceleration.y * sensV, -1, 1);
		float androidMoveHorizontal = Mathf.Clamp(currentAcceleration.x * sensH, -1, 1);

		Vector3 movement = new Vector3 (androidMoveHorizontal, 0, androidMoveVertical);
		rigidbody.AddForce (movement * speed * Time.deltaTime);
	}
	/// <summary>
	/// Moves using the keyboard.
	/// </summary>
	void moveKeyboard(){

		float moveVertical = Input.GetAxis("Vertical");
		float moveHorizontal = Input.GetAxis("Horizontal");
		Vector3 movement = new Vector3 (moveHorizontal, 0, moveVertical);
		rigidbody.AddForce (movement * speed * Time.deltaTime *5);
	}
	/// <summary>
	/// Moves using leap motion.
	/// </summary>
	void moveLeap(){

		float moveVertical = LeapInput.GetHandAxis("Depth");
		float moveHorizontal = LeapInput.GetHandAxis("Horizontal");
		if (LeapInput.GetHandGesture ("TwoFingers")) {

		}
		Vector3 movement = new Vector3 (moveHorizontal, 0, moveVertical);
		rigidbody.AddForce (movement * speed * Time.deltaTime * 1.5F);

	}
	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter (Collider other) {

		other.gameObject.transform.position = Manager.createRandomVector3();
		if (other.gameObject.tag == "Pickup") {
			pickupCollision ();

		} else if (other.gameObject.tag == "BadPickup") {
		
			badPickupCollision (other);
		
		}
	}
	/// <summary>
	/// Handles the bad pickup collision.
	/// </summary>
	/// <param name="other">Other.</param>
	void badPickupCollision(Collider other){
		badpickupSound.Play ();

		float newX = (float)transform.localScale.x / (float)2;
		float newY = (float)transform.localScale.y / (float)2;
		float newZ = (float)transform.localScale.z / (float)2;
		transform.localScale = new Vector3 (newX, newY, newZ);
		other.gameObject.transform.localScale += new Vector3 (0.6F, 0.6F, 0.6F);


		hasLost ();
		
	}

	/// <summary>
	/// Handles the "good" Pickups collision.
	/// </summary>
	void pickupCollision(){
		pickupSound.Play ();

		float newX = (float)transform.localScale.x + (float)0.4;
		float newY = (float)transform.localScale.y + (float)0.4;
		float newZ = (float)transform.localScale.z + (float)0.4;
		transform.localScale = new Vector3 (newX, newY, newZ);
		score = score + 5;
		SetScoreText ();
		hasLost ();

	}

	/// <summary>
	/// Instantiates the good pickups.
	/// </summary>
	void InstantiateGoodPickup(){
		for (int i=0; i<maxGoodPickup; i++) {
			Instantiate(Manager.gp);
		}
	
	}
	/// <summary>
	/// Instantiates the bad pickups.
	/// </summary>
	void InstantiateBadPickup(){
		if (enemyCount < maxEnemyCount) {
			Instantiate (Manager.bp);
			enemyCount++;
		}
	}
	/// <summary>
	/// Sets the score text.
	/// </summary>
	void SetScoreText(){
		countText.text = "Score: " + score.ToString();

	}

	/// <summary>
	/// Checks if the player has lost the game.
	/// </summary>
	void hasLost(){

		float radius = transform.localScale.x;
		if (radius <= minSize) {
			winText.text = "YOU LOST THE GAME" + "\n";
			updateHighScore (playerName, score);

			backToMainMenu();
		}

	}
	/// <summary>
	/// Backs to main menu.
	/// </summary>
	void backToMainMenu(){
			Application.LoadLevel("mainMenu");	
	}



	// Adds the player to the leaderboards and shifts the necessary scores
	void updateHighScore(string pName, int sco){
		for (int i = 1; i<=5; i++) {
			if(!PlayerPrefs.HasKey(i+"SurvivalScore")){
				PlayerPrefs.SetInt (i+"SurvivalScore", sco);
				PlayerPrefs.SetString (i+"SurvivalName", pName);
				Debug.LogWarning(" Adding NEW  " + i + " player " + pName + " with score " + sco); 
				break;
			}
			// If score is better than the previous one
			if(sco > PlayerPrefs.GetInt(i+"SurvivalScore")){
				int oldScore = PlayerPrefs.GetInt(i+"SurvivalScore");
				string oldName = PlayerPrefs.GetString(i+"SurvivalName");
				PlayerPrefs.SetInt (i+"SurvivalScore", sco);
				PlayerPrefs.SetString (i+"SurvivalName", pName);
				Debug.LogWarning(" Replacing " + i + " with player " + pName + " and score " + sco); 
				updateHighScore(oldName,oldScore);
				break;
			}

		}

	}


	


}
